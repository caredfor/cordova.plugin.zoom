package cordova.plugin.zoom;

import java.util.concurrent.Callable;

import android.util.Log;
import android.os.Bundle;

import us.zoom.sdk.MeetingActivity;

public class ZoomActivity extends MeetingActivity {

    private static final String TAG = "<----- ZoomActivity ----->";
    private static final boolean DEBUG = true;

    @Override
    public void onBackPressed() {
        if(DEBUG) {
            Log.v(TAG, "***** onBackPressed caught, calling onClickLeave ");
        }
        onClickLeave();
    }
}